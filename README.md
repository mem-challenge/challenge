# Memory Web-UI Challenge
Let's suppose that you are part of a development team that is currently developing a profiler for Java applications.
Your Job is to design the Web UI that will be used to visualize the data that is held in memory by the Java application.

You are supposed to design/develop a Sharp and Smooth Web-UI for this solely purpose.

## Web UI Requirements

![Memory Block Image](memory-block.png "Memory Block")

The Web-UI should present a Grid where each cell represents a byte of memory. 
Allocated memory cells must be grouped, colored and labelled. 
Non-allocated memory cells must all look the same.

A group of two or more cells is denominated region. A region can contain other regions that contain other regions recursively.

Hovering on a region must display more information about it, such as:
* The Size of the Memory Region
* The Data Type of the Memory Region

Memory regions with the same Data Type must have the same color.

Clicking on a memory region must display it's contents in two forms:
* binary - display the byte value of each memory cell
* text - display the textual representation of the binary data in that memory region

### Desired UI features
* Beauty
* Animations

## Technical Requirements
* UI Framework: Vuejs
* HTTP Client: axios
* Build tool: Webpack
* JavaScript version: >= ES6
* JavaScript Transpiler: Babel
* CSS Preprocessor : none
* Use Vue Single File Components
* Write Unit Tests

## Data
The data to be presented on the Web-UI can be found in this [Gitlab Repository](https://gitlab.com/mem-challenge/memory-json)